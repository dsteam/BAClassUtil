//
//  ViewController.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "ViewController.h"
#import "Company.h"
#import "Country.h"
#import "BACopy.h"
#import <objc/runtime.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    Company *company = [Company new];
    Country *country = [Country new];
    Culture *cultrue = [Culture new];
    cultrue.type = 1;
    cultrue.name = @"china";
    
    company.name = @"123456";
    company.count = @(234234242);
    company.number = 100;
    company.str1 = @"这是str1";
    company.cultrue = cultrue;
    
    [BACopy copy:company to:country];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"Company name: %@, count: %@, number: %@, str1: %@, cultrue.type: %@, cultrue.name: %@",
          company.name, company.count, @(company.number), company.str1,  @(company.cultrue.type), company.cultrue.name);
    NSLog(@"country name: %@, count: %@, number: %@, str1: %@, cultrue.type: %@, cultrue.name: %@",
          country.name, country.count, @(country.number), country.str1,  @(country.cultrue.type), country.cultrue.name);
    NSLog(@"%p", country.cultrue);
    NSLog(@"%p", company.cultrue);
    
    cultrue.name = @"USA";
    cultrue.type = 1;
    NSLog(@"cultrue.type: %@, cultrue.name: %@", @(company.cultrue.type), company.cultrue.name);
    NSLog(@"cultrue.type: %@, cultrue.name: %@", @(country.cultrue.type), country.cultrue.name);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
