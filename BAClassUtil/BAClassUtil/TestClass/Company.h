//
//  Company.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/17.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Test.h"
#import "TestProtocol1.h"
#import "Culture.h"

@interface Company : Test<TestProtocol1>
@property (nonatomic, assign) NSInteger number;
@property (nonatomic, strong) Culture *cultrue;
@property (nonatomic, assign) char *chr;
@property (nonatomic, assign) int *pint;
@property (nonatomic, copy) void (^block)(void );
@end
