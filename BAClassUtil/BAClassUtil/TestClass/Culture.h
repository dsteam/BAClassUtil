//
//  Culture.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/30.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Culture : NSObject
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString *name;

@end
