//
//  Test.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestProtocol.h"

@interface Test : NSObject<TestProtocol>
@property (nonatomic, copy) NSString *str1;
@property (nonatomic, copy) NSString *str2;
@property (nonatomic, copy) NSString *str3;
@property (nonatomic, copy) NSString *str4;
@property (nonatomic, copy) NSString *str5;

@end
