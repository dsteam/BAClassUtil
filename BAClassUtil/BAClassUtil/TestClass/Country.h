//
//  Country.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/17.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Test.h"
#import "TestProtocol1.h"
#import "Culture.h"

@interface Country : Test<TestProtocol1>
@property (nonatomic, assign, readonly) NSInteger number;
@property (nonatomic, strong) Culture *cultrue;

@end
