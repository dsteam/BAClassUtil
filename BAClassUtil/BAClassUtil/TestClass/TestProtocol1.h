//
//  TestProtocol1.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TestProtocol1<NSObject>
@property (nonatomic, strong) NSNumber *count;

@end
