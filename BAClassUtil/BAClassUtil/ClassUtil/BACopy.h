//
//  BACopy.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BACopy : NSObject
+(void )copy:(NSObject *)sourceObj to:(NSObject *)targetObj;
+(void )copy:(NSObject *)sourceObj to:(NSObject *)targetObj ignore:(NSArray<NSString *> *)propertyNames;
+(void )deepCopy:(NSObject *)sourceObj to:(NSObject *)targetObj;
+(void )deepCopy:(NSObject *)sourceObj to:(NSObject *)targetObj ignore:(NSArray<NSString *> *)propertyNames;

@end
