//
//  NSObject+BAModel.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/30.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "NSObject+BAModel.h"

@implementation NSObject (BAModel)
-(void )modelWithDictionry:(NSDictionary *)dict
{
    [self modelWithObject:dict];
}

-(void )modelWithArray:(NSArray *)array
{
    [self modelWithObject:array];
}

-(void )modelWithJson:(NSString *)json
{
    [self modelWithObject:json];
}

-(void )modelWithObject:(id )object
{
    
}
@end

@implementation NSArray (BAModel)
-(NSArray *)modelWithClass:(Class )cls
{
    return nil;
}

+(NSArray *)modelWithArray:(NSArray *)array class:(Class)cls
{
    return [array modelWithClass:cls];
}
@end
