//
//  BAMethodInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/26.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface BAMethodInfo : NSObject
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, assign, readonly) char *types;
@property (nonatomic, assign, readonly) IMP imp;
@property (nonatomic, assign, readonly) SEL sel;
@property (nonatomic, assign, readonly) char *returnType;
@property (nonatomic, assign, readonly) char *argumentType;

+(instancetype )MethodInfoWith:(Method )method;
@end
