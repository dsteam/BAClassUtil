//
//  BAProtocolInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAPropertyInfo.h"
#import "BAMethodInfo.h"

@interface BAProtocolInfo : NSObject
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSArray<Protocol *> *confirmProtocols;
@property (nonatomic, strong, readonly) NSArray<BAPropertyInfo *> *propertys;
@property (nonatomic, strong, readonly) NSArray<BAMethodInfo *> *methods;

+(instancetype )ProtocolInfoWith:(Protocol *)protocol;
@end
