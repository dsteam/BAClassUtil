//
//  BAIvarInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/27.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface BAIvarInfo : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) ptrdiff_t offset;

+(instancetype )IvarInfoWIth:(Ivar )ivar;
@end
