//
//  BAClassCache.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAClassCache.h"
static NSMutableDictionary *_class_cache_dict_ = nil;

@implementation BAClassCache
+(void )saveClassInfo:(BAClassInfo *)classInfo
{
    if (_class_cache_dict_ == nil)
    {
#warning todo 这里有线程安全问题
        _class_cache_dict_ = [NSMutableDictionary dictionary];
    }
    
    //check
    [[self class] checkClassInfo:classInfo];
    
    //cache
    NSString *cacheKey = NSStringFromClass(classInfo.cls);
    if ([_class_cache_dict_ objectForKey:cacheKey] == nil)
    {
        _class_cache_dict_[cacheKey] = classInfo;
    }
}

+(BAClassInfo *)classInfoFrom:(Class )cls
{
    NSAssert(cls != nil, @"class can't be nil");
    return _class_cache_dict_[NSStringFromClass(cls)];
}

+(void )checkClassInfo:(BAClassInfo *)classInfo
{
    NSAssert(classInfo.cls != nil, @"class can't be nil");
}

+(void )cleanCacheFrom:(Class )cls
{
    NSAssert(cls != nil, @"class can't be nil");
    [_class_cache_dict_ removeObjectForKey:NSStringFromClass(cls)];
}

+(void )cleanAllCache
{
    [_class_cache_dict_ removeAllObjects];
}
@end
