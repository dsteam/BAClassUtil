//
//  BAIvarInfo.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/27.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAIvarInfo.h"

@implementation BAIvarInfo
+(instancetype )IvarInfoWIth:(Ivar )ivar
{
    BAIvarInfo *ivarInfo = [BAIvarInfo new];
    ivarInfo.name = [NSString stringWithUTF8String:ivar_getName(ivar)];
    ivarInfo.type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
    ivarInfo.offset = ivar_getOffset(ivar);
    return ivarInfo;
}

@end
