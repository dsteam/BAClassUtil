//
//  BAMethodInfo.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/26.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAMethodInfo.h"

@interface BAMethodInfo()
@property (nonatomic, assign) struct objc_method_description *m_desc;

@end

@implementation BAMethodInfo
+(instancetype )MethodInfoWith:(Method )method
{
    return [[[self class] alloc] initWithMethod:method];
}

-(instancetype )initWithMethod:(Method )method
{
    if (self = [super init])
    {
        [self parsingMethod:method];
    }
    return self;
}

-(void )parsingMethod:(Method )method
{
    struct objc_method_description *m_desc = method_getDescription(method);
    self.m_desc = m_desc;
    self->_name = NSStringFromSelector(m_desc->name);
    self->_types = m_desc->types;
    self->_sel = m_desc->name;
    self->_imp = method_getImplementation(method);
}
@end
