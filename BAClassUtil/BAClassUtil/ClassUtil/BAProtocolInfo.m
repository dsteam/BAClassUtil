//
//  BAProtocolInfo.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAProtocolInfo.h"

@interface BAProtocolInfo()
@property (nonatomic, strong, readwrite) NSArray<Protocol *> *confirmProtocols;
@property (nonatomic, strong, readwrite) NSArray<BAPropertyInfo *> *propertys;
@property (nonatomic, strong, readwrite) NSArray<BAMethodInfo *> *methods;
@end

@implementation BAProtocolInfo
+(instancetype )ProtocolInfoWith:(Protocol *)protocol
{
    return [[[self class] alloc] initWithProtocol:protocol];
}

-(instancetype )initWithProtocol:(Protocol *)protocol
{
    if (self = [super init])
    {
        self->_name = [NSString stringWithUTF8String:protocol_getName(protocol)];
        [self parsingProtocol:protocol];
    }
    return self;
}

-(void )parsingProtocol:(Protocol *)protocol
{
    //获取protocol 上的属性
    [self parsingProperty:protocol];
    
    //获取protocol实现的协议
    [self parsingConfirmProtocol:protocol];
    
    //获取protocol方法
    [self parsingMethod:protocol];
}

-(void )parsingProperty:(Protocol *)protocol
{
    NSMutableArray<BAPropertyInfo *> *propertys = [NSMutableArray array];
    unsigned propertyCount = 0;
    objc_property_t *propertyList = protocol_copyPropertyList(protocol, &propertyCount);
    for (int i = 0; i < propertyCount; ++i)
    {
        BAPropertyInfo *propertyInfo = [BAPropertyInfo PropertyInfoWith:propertyList[i]];
        [propertys addObject:propertyInfo];
    }
    free(propertyList);
    self.propertys = [propertys copy];
}


-(void )parsingConfirmProtocol:(Protocol *)protocol
{
    NSMutableArray *confirmProtocols = [NSMutableArray array];
    unsigned protocolCount = 0;
    Protocol *__unsafe_unretained * protocolList = protocol_copyProtocolList(protocol, &protocolCount);
    if (protocolList != NULL)
    {
        for (int i = 0; i < protocolCount; ++i)
        {
            [confirmProtocols addObject:protocolList[i]];
        }
        free(protocolList);
        self.confirmProtocols = [confirmProtocols copy];
    }
}

-(void )parsingMethod:(Protocol *)protocol
{
    NSMutableArray *methods = [NSMutableArray array];
    unsigned methodCount = 0;
    
    for (int i = 0; i < methodCount; ++i)
    {
        
    }
    
    self.methods = [methods copy];
}
@end
