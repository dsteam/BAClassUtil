//
//  BAMetaClassInfo.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/28.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAMetaClassInfo.h"

@implementation BAMetaClassInfo
+(instancetype )MetaClassInfoWithClassInfo:(BAClassInfo *)classInfo
{
    return [[[self class] alloc] initWithClassInfo:classInfo];
}

+(instancetype )MetaClassInfoWithClass:(Class )cls
{
    return [[self class] MetaClassInfoWithClassInfo:[BAClassInfo ClassInfoWithClass:cls]];
}

-(instancetype )initWithClassInfo:(BAClassInfo *)classInfo
{
    if (self = [super init])
    {
        [self parsingClassInfo:classInfo];
    }
    return self;
}

-(void )parsingClassInfo:(BAClassInfo *)classInfo
{
    self->_classInfo = classInfo;
    self->_allProperty = [self getPropertyFromClassInfo:classInfo];
}

-(NSArray<BAPropertyInfo *> *)getPropertyFromClassInfo:(BAClassInfo *)classInfo
{
    NSMutableArray<BAPropertyInfo *> *propertys = [NSMutableArray arrayWithArray:classInfo.propertys];
    if (classInfo.superClassInfo != nil)
    {
        NSArray<BAPropertyInfo *> *superPropertys = [self getPropertyFromClassInfo:classInfo.superClassInfo];
        if (superPropertys != nil
            && superPropertys.count > 0)
        {
            [propertys addObjectsFromArray:superPropertys];
        }
    }
    return [propertys copy];
}
@end
