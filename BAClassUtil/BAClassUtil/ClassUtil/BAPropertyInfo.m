//
//  BAPropertyInfo.m
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import "BAPropertyInfo.h"


@implementation BAPropertyInfo
+(instancetype )PropertyInfoWith:(objc_property_t )property
{
    return [[[self class] alloc] initWithProperty:property];
}

-(instancetype )initWithProperty:(objc_property_t)property
{
    if (self = [super init])
    {
        [self parsingProperty:property];
    }
    return self;
}

-(void )parsingProperty:(objc_property_t )property
{
    self->_name = [NSString stringWithUTF8String:property_getName(property)];
    unsigned int propertyOutCount = 0;
    objc_property_attribute_t *propertyAttributeList = property_copyAttributeList(property, &propertyOutCount);
//    for (unsigned j = 0; j < propertyOutCount; ++j)
//    {
        objc_property_attribute_t propertyAttribute = propertyAttributeList[0];
        NSLog(@"name:%s value:%s", propertyAttribute.name, propertyAttribute.value);
//    }

    free(propertyAttributeList);
}
@end
