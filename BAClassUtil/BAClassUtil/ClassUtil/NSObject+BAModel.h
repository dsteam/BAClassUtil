//
//  NSObject+BAModel.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/30.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (BAModel)

-(void )modelWithDictionry:(NSDictionary *)dict;
-(void )modelWithJson:(NSString *)json;
-(void )modelWithObject:(id )object;
@end

@interface NSArray (BAModel)
-(NSArray *)modelWithClass:(Class )cls;
+(NSArray *)modelWithArray:(NSArray *)array class:(Class )cls;
@end
