//
//  BAClassCache.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAClassInfo.h"

@interface BAClassCache : NSObject
+(void )saveClassInfo:(BAClassInfo *)classInfo;
+(BAClassInfo *)classInfoFrom:(Class )cls;

+(void )cleanCacheFrom:(Class )cls;
+(void )cleanAllCache;
@end
