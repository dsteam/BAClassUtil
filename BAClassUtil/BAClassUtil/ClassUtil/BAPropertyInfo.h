//
//  BAPropertyInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "BAPropertyType.h"


@interface BAPropertyInfo : NSObject
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, assign) BAPropertyType propertyType;

+(instancetype )PropertyInfoWith:(objc_property_t )property;

@end
