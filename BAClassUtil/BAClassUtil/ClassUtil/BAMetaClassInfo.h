//
//  BAMetaClassInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/28.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAClassInfo.h"

@interface BAMetaClassInfo : NSObject
@property (nonatomic, strong, readonly) BAClassInfo *classInfo;
@property (nonatomic, strong, readonly) NSArray<BAPropertyInfo *> *allProperty;
@property (nonatomic, strong, readonly) NSArray<BAIvarInfo *> *allIvar;
@property (nonatomic, strong, readonly) NSArray<BAProtocolInfo *> *allConfirmProtocol;
@property (nonatomic, strong, readonly) NSArray<BAMethodInfo *> *allMethod;

+(instancetype )MetaClassInfoWithClassInfo:(BAClassInfo *)classInfo;
+(instancetype )MetaClassInfoWithClass:(Class )cls;
@end
