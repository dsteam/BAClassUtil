//
//  BAClassInfo.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/24.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAPropertyInfo.h"
#import "BAProtocolInfo.h"
#import "BAMethodInfo.h"
#import "BAIvarInfo.h"

@interface BAClassInfo : NSObject
@property (nonatomic, assign, readonly) Class cls;
@property (nonatomic, assign, readonly) Class supercls;
@property (nonatomic, strong, readonly) BAClassInfo *superClassInfo;

@property (nonatomic, strong, readonly) NSArray<BAPropertyInfo *> *propertys;
@property (nonatomic, strong, readonly) NSArray<BAProtocolInfo *> *protocols;
@property (nonatomic, strong, readonly) NSArray<BAMethodInfo *> *methods;
@property (nonatomic, strong, readonly) NSArray<BAIvarInfo *> *ivars;

+(instancetype )ClassInfoWithClass:(Class )cls;
@end
