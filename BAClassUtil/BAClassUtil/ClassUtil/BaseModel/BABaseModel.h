//
//  BABaseModel.h
//  BAClassUtil
//
//  Created by pdc on 2017/9/30.
//  Copyright © 2017年 RealsCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BABaseModel : NSObject

+(instancetype )modelWithDictionry:(NSDictionary *)dict;
+(instancetype )modelWithJson:(NSString *)json;
+(instancetype )modelWithObject:(id )object;
@end
